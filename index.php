<?php

require_once "src/Employee.php";
require_once "src/User.php";
require_once "src/Rectangle.php";
require_once "src/Student.php";

/*Задачи 1.1 - 1.5*/
$human = new Employee();

$human->name = "Иван";
$human->age = 25;
$human->salary = 1000;

$human2 = new Employee();
$human2->name = "Вася";
$human2->age = 26;
$human2->salary = 2000;

echo "Сумма зарплат: " , $human->salary + $human2->salary;
echo "<br>";
echo "Сумма возрастов: " , $human->age + $human2->age;
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*Задачи 2.1 - 2.6*/
echo "Выводим имя: " . $human->getName();
echo "<br>";
echo "Выводим возраст: " . $human->getAge();
echo "<br>";
echo "Выводим зарплату: " . $human->getSalary();
echo "<br>";
echo "Проверяем возраст: " . $human2->checkAge();
echo "<br>";

$human3 = new Employee();
$human3->salary = 1500;

$human4 = new Employee();
$human4->salary = 2300;
echo 'Сумма зарплат работников: ' , $human3->getSalary() + $human4->getSalary();
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*Задачи 3.1 - 3.4*/
$user = new User();
$user->name = 'Коля';
$user->age = 25;
echo 'Возраст Коли: ' . $user->setAge(30);
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*Задача 4.1*/
$human5 = new Employee();
$human5->salary = 1200;
echo "Увеличенная зарплата в двое: " . $human5->doubleSalary();
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*Задачи 5.1 - 5.3*/
$rectangle = new Rectangle();
$rectangle->width=20;
$rectangle->height=5;
echo "Площадь прямоугольника: ". $rectangle->getSquare();
echo "<br>";
echo "Периметр прямоугольника: ".$rectangle->getPerimeter();
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*Задачи 5.1 - 5.3*/
$user2 = new User();
$user2->age=25;
echo $user2->setAge(32)."<br>";
echo $user2->addAge(13)."<br>";
echo $user2->subAge(15)."<br>";

$user3 = new User();
$user3->age=32;
$user3->setAge2(20);
echo $user3->age;
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*задачи 7.1 - 7.2*/
$user3 = new User();
$user3->age=30;
$user3->setAge2(18); //Error
echo $user3->age;
echo "<br>/************************************************************************/<br>";
/************************************************************************/

/*задачи 8.1 - 8.8*/
$student = new Student();
$student->name = '';
$student->course = '';
$student->courseAdministrator = 'Иванов'; //use __set
echo $student->courseAdministrator; //use __get
echo "<br>/************************************************************************/<br>";
/************************************************************************/
