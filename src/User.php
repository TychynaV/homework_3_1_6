<?php

class User
{
    public $name;

    public $age;

    /**
     * @param $age
     * @return int
     */
    public function setAge($age)
    {
        if ($age >= 18) {
            return $this->age = $age;
        } else {
            return $this->age;
        }
    }

    public function setAge2($age)
    {
        if($this->validData($age) === true)
            $this->age = $age;
    }

    /**
     * @param $count
     * @return int
     */
    public function addAge($count)
    {
        return $this->age + $count;
    }

    /***
     * @param $count
     * @return int
     */
    public function subAge($count)
    {
        return $this->age - $count;
    }

    private function validData($data): bool
    {
        return is_string($data) && $data >= 18;
    }
}
