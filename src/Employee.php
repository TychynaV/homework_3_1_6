<?php

class Employee
{
    public $name;

    public $age;

    public $salary;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @return bool
     */
    public function checkAge()
    {
        return $this->age > 18;
    }

    /**
     * @return int
     */
    public function doubleSalary()
    {
        return $this->salary * 2;
    }
}
