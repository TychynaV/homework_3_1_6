<?php

class Rectangle
{
    public $width;

    public $height;

    /**
     * @return int
     */
    public function getSquare()
    {
        return $this->width * $this->height;
    }

    /**
     * @return int
     */
    public function getPerimeter()
    {
        return ($this->width + $this->height) * 2;
    }
}
