<?php

class Student
{
    public $name;

    public $course;

    private $courseAdministrator;

    public function transferToNextCourse($name, $course)
    {
        if($this->isCourseCorrect($course)) {
            $this->course = $course;
        }
    }
    private function isCourseCorrect($data): bool
    {
        return $this->course>$data || $data <= 5;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
